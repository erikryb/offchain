Offchain contracts
==================

This is an exploration of ideas on how to do contracts off the ethereum
blockchain.

Structure of the paper
----------------------
The paper should go through multiple iterations of the idea, with motivation
along the way.

1. How to write a chess contract on ethereum
2. How to improve this contract by documenting the moves on ethereum, but
having the contract offchain.
3. How to convert any ethereum contract to an offchain contract.

Random ideas
------------

* Zerocoin?

What is an offchain contract?
-----------------------------

An offchain contract is a piece of code taking as input a history and a
withdrawal claim, and giving a truth value as output.

Ethereum contracts
------------------
 
The following contracts are needed in Ethereum:

* History
* Escrow

### History

This contract keeps a history that the escrow can use as input for the
withdrawal claim verifications.

Pseudocode:

    if msg.text[0] == "save":
	storage[storage[0]] <- (timestamp, msg.sender, msg.text[1:])
	storage[0] += 1
    if msg.text[0] == "load":
        return storage[msg.text[1]]

### Escrow

This contract acts as an escrow. Anyone can send money to an escrow account. To
get money out of an escrow account a user needs to send a proof that his
withdrawal claim is valid.

All payments to and withdrawals from the escrow accounts gets recorded in the
history contract.

Pseudocode:

    if msg.text[0] == "send":
        storage[msg.text[1]] += msg.value
    if msg.text[0] == "withdrawal"
        account = msg.text[1]
	value = msg.text[2]
	proof = msg.text[3]
	if SHA3(proof.f) == account and isValid(proof, history, value, msg.sender):
	    storage[account] -= msg.value
	    send value to msg.sender
        
## Withdrawal claim
A withdrawal claim consists of

1. Escrow account
2. A withdrawal value

## Withdrawal proof
A proof of the validity of a withdrawal claim consists of:

* A function f: (history, withdrawal claim) -> Bool

A proof that the following is satisfied:

* f(history, withdrawal claim) = True

Splitting an offline contract
-----------------------------
Should be able to split an offline contract into a tree, so that only parts of
it is needed for withdrawal verification. One solution is to put multiple
offline contracts in a merkle tree, and let the verification yield true if
just one of the contracts yields true.

Simulation of ordinary ethereum contracts in offchain contracts
---------------------------------------------------------------

A class of somewhat pure ethereum contracts:
These contracts can be described as functions
(timestamp, msg.sender, msg.value, msg.text, storage, balance) -> (storage, balance, sentValues)

Given one such contract C, we can make the following offline contract:

    sentValues <- simulate(C,history)
    if value <= sentValues[recipient]:
        return True
    else:
        return False


Examples of offchain contracts
------------------------------

### Betting contract

Pseudocode:

    guesses <- filter ("Guess by recipient before result") history
    if length guesses == 0:
        balance <- "Sum of bets by recipient"
	if value <= balance:
	    return True
	else:
	    return False
    guess <- head guesses
    result <- head $ filter ("Result") history
    if guess /= result:
        return False
    else:
        balance <- totalbalance * ("Bet by recipient" / "Sum of correct bets")
	if value < balance:
	    return True
	else:
	    return False

### Coinflip

Pseudocode:

    A = "A's account"
    B = "B's account"
    winner <- findWinner A B history
    if recipient == winner:
        return True
    else:
        return False
