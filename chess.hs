data Account = Int
send :: Double -> Account -> Eth ()

twoEscrow :: Eth (Account, Account, Double)
playChess :: Account -> Account -> Eth Account

main :: Eth ()
main = do
    (player1, player2, prize) <- twoEscrow
    winner <- playChess player1 player2
    send prize winner
